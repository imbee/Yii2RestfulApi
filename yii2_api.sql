/*
Navicat MySQL Data Transfer

Source Server         : localhost_3306
Source Server Version : 50714
Source Host           : localhost:3306
Source Database       : pear

Target Server Type    : MYSQL
Target Server Version : 50714
File Encoding         : 65001

Date: 2022-02-08 16:56:29
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for yp_order
-- ----------------------------
DROP TABLE IF EXISTS `yp_order`;
CREATE TABLE `yp_order` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order_num` varchar(20) CHARACTER SET latin1 DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `pay_status` tinyint(2) DEFAULT '0' COMMENT '0: 未支付 1：已支付',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yp_order
-- ----------------------------
INSERT INTO `yp_order` VALUES ('1', '92001123', '12', '0');
INSERT INTO `yp_order` VALUES ('2', '9200112711', '1', '1');
INSERT INTO `yp_order` VALUES ('3', '92001125', '1', '0');
INSERT INTO `yp_order` VALUES ('5', '92001126', '2', '0');
INSERT INTO `yp_order` VALUES ('6', '92001127', '2', '0');
INSERT INTO `yp_order` VALUES ('7', '1', null, '0');
INSERT INTO `yp_order` VALUES ('11', '2021', '22', '0');
INSERT INTO `yp_order` VALUES ('12', '2021', '22', '0');
INSERT INTO `yp_order` VALUES ('13', '2021', '22', '0');
