<?php

namespace api\models\resource;

use api\models\Order;
use Yii;
use yii\base\Model;
use yii\web\Link;
use yii\web\Linkable;
use yii\helpers\Url;

/**
 * This is the model class for table "yp_order".
 *
 * @property int $id
 * @property string|null $order_num
 * @property int|null $user_id
 * @property int|null $pay_status 0: 未支付 1：已支付
 */
class OrderResource extends Order implements  Linkable
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'yp_order';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'pay_status'], 'integer'],
            [['order_num'], 'string', 'max' => 20],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'order_num' => 'Order Num',
            'user_id' => 'User ID',
            'pay_status' => 'Pay Status',
        ];
    }

    public function fields()
    {
        return ['user_id',
            'age'=>function($model){
                return $model->id.':'.$model->pay_status;
            }
        ];
    }

    public function extraFields()
    {
        return [
            'profile'=>function($model){
                return [
                    'id'=>$model->id,
                    'name'=>$model->order_num,
                ];
            }
        ];
    }


    /**
     * @inheritDoc
     */
    public function getLinks()
    {
        return [
            Link::REL_SELF => Url::to(['order/view', 'id' => $this->id], true),
            'edit' => Url::to(['order/view', 'id' => $this->id], true),
            'profile' => Url::to(['order/view', 'id' => $this->id], true),
            'index' => Url::to(['order'], true),
        ];
    }
}
