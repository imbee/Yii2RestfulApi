<?php

namespace api\models;

use Yii;
use yii\helpers\Url;
use yii\web\Link;
use yii\web\Linkable;

/**
 * This is the model class for table "yp_order".
 *
 * @property int $id
 * @property string|null $order_num
 * @property int|null $user_id
 * @property int|null $pay_status 0: 未支付 1：已支付
 */
class Order extends \yii\db\ActiveRecord implements Linkable
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'yp_order';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'pay_status'], 'integer'],
            [['order_num'], 'string', 'max' => 20],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'order_num' => 'Order Num',
            'user_id' => 'User ID',
            'pay_status' => 'Pay Status',
        ];
    }

    public function fields()
    {
       $fields = parent::fields();
       unset($fields['pay_status']);    // 指定某些字段不用返回
       return $fields;
    }

    public function extraFields()
    {
        return [
            'more'=>function($model){
                    return [
                        'pay_status-cn'=>$model->pay_status == 0 ?'未支付':'已支付'
                    ];
                }
        ];
    }

    /**
     * @inheritDoc
     * 用于展示产品的链接，可删除
     */
    public function getLinks()
    {
        // TODO: Implement getLinks() method.
        return [
            Link::REL_SELF=>Url::to(['order/view/'.$this->id],true),
        ];
    }
}
