<?php
$params = array_merge(
    require __DIR__ . '/../../common/config/params.php',
    require __DIR__ . '/params.php'
);

return [
    'id' => 'app-api',
    'basePath' => dirname(__DIR__),
    'controllerNamespace' => 'api\controllers',
    'bootstrap' => ['log'],
    'modules' => [],
    'components' => [
        'request' => [
            'csrfParam' => '_csrf-backend',
            'enableCookieValidation' => false,
            'enableCsrfValidation' => false,
            'parsers' => [
                'application/json' => 'yii\web\JsonParser',
            ]
        ],
        'user' => [
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => false,
          //  'identityCookie' => ['name' => '_identity-backend', 'httpOnly' => true],      提示：API用户无法通过session来维持登录状态
        ],
        /*'session' => [
            // this is the name of the session cookie used for login on the backend
            'name' => 'advanced-backend',
        ],*/
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'db' => [
            'class' => 'yii\db\Connection',
            'dsn' => 'mysql:host=localhost;dbname=pear',
            'username' => 'root',
            'password' => '',
            'charset' => 'utf8',
            'tablePrefix'=>'yp_'
        ],
        /*
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
            ],
        ],
        */
        'urlManager' => [
            'enablePrettyUrl' => true,
            'enableStrictParsing' => true,
            'showScriptName' => false,
            'rules' => [
                [
                    'class' => 'yii\rest\UrlRule',
                    'controller' => 'order',
                    'ruleConfig'=>[
                        'class'=>'yii\web\UrlRule',
                        'defaults'=>[
                            'expand'=>'more'    // new Order()->extraFields
                        ],
                    ],
                    'patterns'=>[
                        'PUT,PATCH update/<id>'=>'update',
                        'DELETE delete/<id>'=>'delete',
                        'GET,HEAD,OPTIONS view/<id>'=>'view',
                        'POST create'=>'create',
                        'GET,HEAD index'=>'index',
                        //'orders/<id>'=>'order/options',
                        //'orders'=>'order/orders',
                        'POST search'=>'search',
                        'GET page'=>'page',
                    ]
                ],
                [
                    'class'=>'yii\rest\UrlRule',
                    'controller' => 'demo',
                    'except'=>['delete','create','update'],
                    'pluralize'=>false, //无需复数
                    'patterns'=>[
                        'GET search'=>'search',
                        'GET delete'=>'delete'
                    ]
                ],[
                    'class'=>'yii\rest\UrlRule',
                    'controller' => 'user',
                    'pluralize'=>false,
                    'patterns'=>[
                        'POST login'=>'login'
                    ]
                ]
            ],
        ]
    ],
    'params' => $params,
];
