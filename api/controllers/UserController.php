<?php
namespace api\controllers;


use api\models\Order;
use api\models\User;
use api\models\ApiLoginForm;
use yii\db\Query;
use yii\rest\Controller;
use yii\rest\ActiveController;

class UserController extends ActiveController
{
    public $modelClass = 'common\models\User';
    public function actionLogin(){
        $model = new ApiLoginForm();
        //$model->username = $_POST['username'];
        //$model->password = $_POST['password'];
        $model->load(\Yii::$app->getRequest()->getBodyParams(),'');

        if($model->login()){
            return ['access_token'=>$model->login()];
        }else{
            $model->validate();
            return $model;
        }

    }

}