<?php
namespace api\controllers;

use yii\rest\Controller;
use Yii;
class WaybillPdfController extends Controller
{
    public function actionCreateWaybillPdf(){
        if(Yii::$app->request->isPost){
            try{
                $post = Yii::$app->request->post();
                $invoice_path =  $_SERVER['DOCUMENT_ROOT'].'/common/pdf/'.$post['invoice_pdf_url'];
                $label_path =  $_SERVER['DOCUMENT_ROOT'].'/common/pdf/'.$post['label_pdf_url'];
                $res['invoice_url'] =  Yii::$app->request->hostInfo.'/common/pdf/'.$post['invoice_pdf_url'];
                $res['pdf_url'] =  Yii::$app->request->hostInfo.'/common/pdf/'.$post['label_pdf_url'];
                if(!is_dir(dirname($invoice_path))){
                    mkdir(dirname($invoice_path), 0777, true);
                }

                if(!is_dir(dirname($label_path))){
                    mkdir(dirname($label_path), 0777, true);
                }

                file_put_contents($label_path,$post['label_pdf']);

                require_once YII_APP_BASE_PATH . '/vendor/TCPDF-main/tcpdf.php';
                $pdf = new \TCPDF('P', 'mm', 'A4', true, 'UTF-8', false);
                $pdf->SetCreator('Davis');
                $pdf->SetAuthor('Davis');
                $pdf->SetTitle('发票');
                $pdf->SetSubject('发票');
                $pdf->SetKeywords('发票');
                $pdf->setPrintHeader(false);
                $pdf->setPrintFooter(false);
                $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
                $pdf->AddPage();

                $pdf->writeHTML($post['invoice_pdf']);
                $pdf->Output($invoice_path, 'F');

                return $res;
            }catch (\Exception $exception){
                return ['code'=>300,'message'=>$exception->getMessage()];
            }

        }
    }

    public function actionTest(){
        echo Yii::$app->request->hostInfo;
    }
}