<?php
namespace api\controllers;

use api\models\User;
use yii\db\Query;
use yii\rest\Controller;

/**
 * 自定义资源
 * */
class DemoController extends Controller
{

    public function actionSearch(){
        $data = (new Query())
            ->from('yp_order')
            ->select(['Count(id) as num','user_id'])
            ->groupBy(['user_id'])
            ->limit(2)
            ->all();
        return $data;
    }

    public function actionDelete(){
        return 1;
    }

}