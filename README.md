<div align="center">
<h1 align="center">
Yii2 Api
</h1>
<h4 align="center">
基于 Yii2 Restful Api 实现的高效接口应用
</h4>

<p align="center">
	<a href="#">
        <img src="https://img.shields.io/badge/php-7.0.10+-green.svg" alt="PHP Version">
    </a>
    <a href="#">
        <img src="https://img.shields.io/badge/mysql-5.7.14+-green.svg" alt="MYSQL Version">
    </a>
</p>
</div>

### 🎃 1.项目简介

Yii2 Api是基于 Yii2 Restful Api  实现的企业级高效接口应用，采用经典的Bearer Token授权认证模式。是一款轻量级、易上手、开发速度快的接口应用程序。

### 📜 2.项目结构

```
api
    assets              资源发布文件
    config              配置文件
    controllers         控制器文件
    models              模型文件
        resource        模型资源文件
    runtime             运行缓存
    test                测试模块
    views               视图文件
    web                 入口目录
common
    config              配置文件
    mail                邮件模板
    models              模型文件
    tests               测试模块
    widgets             小部件
console
    config              配置文件
    controllers         控制器文件
    migrations          数据库迁移文件
    models              模型文件
    runtime             运行缓存
environments            环境文件
vendor                  composer安装文件
yii2_api.sql            数据库文件

```

### 🔧 3.安装配置
* 配置项目  
  windows配置:
    ```
        <VirtualHost *:80>
        		ServerName yii2api.com
        		ServerAlias yii2api.com
        		DocumentRoot D:/wamp/www/YourProject/api/web
        		<Directory  D:/wamp/www/YourProject/api/web>
        			Options +Indexes +Includes +FollowSymLinks +MultiViews
        			AllowOverride All
        			Require local
        		</Directory>
        	</VirtualHost>
    ```
  nginx配置:
  ```
      listen 80;
      server_name yii2api.com;
      index index.php index.html index.htm default.php default.htm default.html;
      root /www/wwwroot/YourProject/api/web;
  ```
* 数据库配置  
  源文件:yii2api.sql  
  配置路径:common/config/main-local.php
* 访问  
  直接请求:yii2api.com

### 🧪 4.测试案例
#### 4.1 授权  
HTTP Request  
>POST /user/login

请求参数  

参数 | 数据类型 | 描述
:---- | :--- | :---
username | String | 用户名称 
password |  String | 用户密码

返回参数

参数 | 数据类型 | 描述
:---- | :--- | :---
access_token | String | 授权token 

请求案例
```
 curl --location --request POST '../user/login' \
 --header 'Content-Type: application/x-www-form-urlencoded' \
 --data-urlencode 'username=tangsan' \
 --data-urlencode 'password=123'
```

响应结果
```
{
     "access_token": "pI-5UMQnm73kaNxjdneGXVpazbELHEfM"
 }
```

#### 4.2 创建订单
HTTP Request  
>POST /orders/create

请求报头

参数 | 描述
:---- | :---
Authorization | Bearer  access_token
Content-Type | application/x-www-form-urlencoded

请求参数  

参数 | 数据类型 | 描述
:---- | :--- | :---
order_num | String | 订单号 
user_id |  String | 用户ID
pay_status |  Int | 支付状态 默认:0

返回参数

参数 | 数据类型 | 描述
:---- | :--- | :---
id | id | 订单ID
order_num | String |  订单号
... | ... | ...

请求案例
```
curl --location --request POST '../orders/create' \
--header 'Authorization: Bearer access_token' \
--header 'Content-Type: application/x-www-form-urlencoded' \
--data-urlencode 'order_num=xx' \
--data-urlencode 'user_id=xx' \
--data-urlencode 'pay_status=x'
```

响应结果
```
{
    "order_num": "2021",
    "user_id": "22",
    "id": 13,
    "more": {
        "pay_status-cn": "未支付"
    },
    "_links": {
        "self": {
            "href": "http://restfulapi.com/order/view?id=13"
        }
    }
}
```

#### 4.3 修改订单
HTTP Request  
>PUT orders/update/\<id>  
>PATCH orders/update/\<id>

请求报头

参数 | 描述
:---- | :---
Authorization | Bearer  access_token
Content-Type | application/x-www-form-urlencoded

请求参数  

参数 | 数据类型 | 描述
:---- | :--- | :---
order_num | String | 订单号 
user_id |  String | 用户ID
pay_status |  Int | 支付状态

返回参数

参数 | 数据类型 | 描述
:---- | :--- | :---
id | id | 订单ID
order_num | String |  订单号
... | ... | ...

请求案例
```
curl --location --request POST '../orders/create' \
--header 'Authorization: Bearer access_token' \
--header 'Content-Type: application/x-www-form-urlencoded' \
--data-urlencode 'order_num=xx' \
--data-urlencode 'user_id=xx' \
--data-urlencode 'pay_status=x'
```

响应结果
```
{
    "id": 2,
    "order_num": "9200112711",
    "user_id": "1",
    "more": {
        "pay_status-cn": "已支付"
    },
    "_links": {
        "self": {
            "href": "http://restfulapi.com/order/view/1"
        }
    }
}
```

#### 4.4 订单分页列表
HTTP Request  
>GET orders/page?page=1&pageSize=5

请求报头

参数 | 描述
:---- | :---
Authorization | Bearer  access_token

请求参数  

参数 | 数据类型 | 描述
:---- | :--- | :---
page | Int | 当前页 
pageSize |  Int | 每页个数

返回参数

参数 | 数据类型 | 描述
:---- | :--- | :---
id | id | 订单ID
order_num | String |  订单号
... | ... | ...

请求案例
```
curl --location --request GET 'http://restfulapi.com/orders/page?page=1&pageSize=5' \
--header 'Authorization: Bearer access_token' \
```

响应结果
```
[
    {
        "id": "1",
        "order_num": "92001123",
        "user_id": "12",
        "pay_status": "0"
    },
    ...
]
```

#### 4.5 订单删除
HTTP Request  
>DELETE  orders/delete/\<id>

请求报头

参数 | 描述
:---- | :---
Authorization | Bearer  access_token

请求案例
```
curl --location --request DELETE 'http://restfulapi.com/orders/delete/<id>' \
--header 'Authorization: Bearer access_token' \
```

响应结果
```
Status:204 No Content
```

#### 4.6 显示末端支持的动词

HTTP Request  
>OPTIONS /orders/view/\<id>

返回参数

参数 | 数据类型 | 描述
:---- | :--- | :---
message | String | 显示末端支持的动词


请求案例
```
curl --location --request OPTIONS 'http://restfulapi.com/orders/view/1' \
```

响应结果
```
{
    "name": "Method Not Allowed",
    "message": "Method Not Allowed. This URL can only handle the following request methods: GET, HEAD.",
    "code": 0,
    "status": 405,
    "type": "yii\\web\\MethodNotAllowedHttpException"
}
```

#### 4.7 获取Http信息

HTTP Request  
>HEAD orders/2

请求案例
```
curl --location --head 'http://restfulapi.com/orders/2' \
```

响应结果
![GitHub](api/web/common/images/case1.png "GitHub,Social Coding")

### 🎯 5.TODO
* 日志的模块的设计
* 文档的完善  